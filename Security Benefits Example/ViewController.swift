//
//  ViewController.swift
//  Security Benefits Example
//
//  Created by Phong Le on 7/21/21.
//

/// Import Alamofire and needed RxSwift frameworks
import UIKit
import RxCocoa
import RxSwift
import Alamofire

class ViewController: UIViewController {
    
    /// Dispose bag for subscribers
    let disposeBag = DisposeBag()
    
    /// Button that will trigger a API call to AWS lambda
    @IBOutlet weak var hitLambdaButton: UIButton! {
        didSet {
            self.hitLambdaButton.rx.tap
                .asDriver()
                .debounce(.nanoseconds(5)) //Debounce taps until 5 nanoseconds
                .drive(onNext: { [unowned self] in
                    /// Make a url call with Alamofire to get the message from the AWS lambda
                    /// We want a JSON response to parse out the meesage that is in the object
                    AF.request(LambdaRouter.helloMark).responseJSON { response in
                        switch response.result {
                        case .success(let results): // On a success we convert the results to a dictionary and pull out the message
                            guard let json = results as? [String:Any], let message = json["message"] as? String else {
                                return // Currently if it fails we do nothing
                            }
                            
                            /// If we get a message back successfully we present a alert and present it to the user.
                            let alert = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            
                            self.present(alert, animated: true, completion: nil)
                            break
                        case .failure(_):
                            break /// Currently if it fails we do nothing
                        }
                    }
                })
                .disposed(by: disposeBag) ///  Add the button driver to a dispose bag 
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
}

