//
//  LambdaRouter.swift
//  Security Benefits Example
//
//  Created by Phong Le on 7/21/21.
//

import Foundation
import Alamofire

/**
    Simple router that setups request 
 */
enum LambdaRouter: URLRequestConvertible  {
    case helloMark
    
    var method: HTTPMethod {
        switch self {
        case .helloMark:
            return .get
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: "https://l82i77sjq8.execute-api.eu-west-1.amazonaws.com/dev/hello-world")
        
        var request = URLRequest(url: url!)
        request.httpMethod = method.rawValue
        
        return request
    }
}
